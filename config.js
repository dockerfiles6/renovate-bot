module.exports = {
  hostRules: [
    {
      hostType: "docker",
      matchHost: "registry.gitlab.com/dockerfiles6",
      username: process.env.DEPLOY_USER,
      password: process.env.DEPLOY_TOKEN,
    },
  ],
  endpoint: "https://gitlab.com/api/v4/",
  platform: "gitlab",
  gitUrl: "https",
  onboardingConfig: {
    extends: ["config:base"],
  },
  vulnerabilityAlerts: {
    labels: ["security"],
    schedule: "at any time",
  },
  repositories: [
    "dockerfiles6/template/ci-build",
    "dockerfiles6/images/hadolint",
    "dockerfiles6/images/ansible-lint",
    "dockerfiles6/images/lastversion",
    "dockerfiles6/images/container-structure-test",
    "dockerfiles6/images/img",
    "b4288/infra-as-code-homelab",
    "ansible-stephrobert/collections/base",
    "dockerfiles6/images/molecule-collections"
  ],
};
